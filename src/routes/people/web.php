<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\People\app\http\controllers\PersonController;
use Yeltrik\People\app\http\controllers\PersonImportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('people',
    [PersonController::class, 'index'])
    ->name('people.index');

Route::get('people/create',
    [PersonController::class, 'create'])
    ->name('people.create');

Route::post('people',
    [PersonController::class, 'store'])
    ->name('people.store');

Route::get('people/{person}',
    [PersonController::class, 'show'])
    ->name('people.show');

Route::get('people/import/csv',
    [PersonImportController::class, 'csv'])
    ->name('people.import.csv');
