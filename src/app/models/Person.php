<?php

namespace Yeltrik\People\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;
use Yeltrik\Data\app\traits\HasData;

class Person extends Model
{
    use HasData;
    use HasFactory;
    use RevisionableTrait;
    use SoftDeletes;

    protected $connection = 'people';
    public $table = 'people';

    protected $revisionCreationsEnabled = true;

    public function name() : string
    {
        return "";
    }
}
