<?php

namespace Yeltrik\People\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class PersonImportController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function csv()
    {
        return view('people::people.import.csv');
    }

}
