<?php

namespace Yeltrik\People\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Yeltrik\People\app\models\Person;

class PersonController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $people = Person::query()->get();

        return view('people::people.index', compact(
            'people'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('people::people.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $person = new Person();
        $person->save();

        return redirect()->route('people.show', $person);
    }

    /**
     * Display the specified resource.
     *
     * @param Person $person
     * @return void
     */
    public function show(Person $person)
    {
        $data = $person->data();
        $model = $person;
        $revisionHistory = $person->revisionHistory;
        return view('people::people.show', compact(
            'person',
            'data',
            'model',
            'revisionHistory'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Person $person
     * @return void
     */
    public function edit(Person $person)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Person $person
     * @return void
     */
    public function update(Request $request, Person $person)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Person $person
     * @return void
     */
    public function destroy(Person $person)
    {
        //
    }
}
