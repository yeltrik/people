<div class="dropdown-menu" aria-labelledby="navbarDropdown">
    @include('people::nav-item.dropdown.menu.item.create')
    @include('people::nav-item.dropdown.menu.item.index')
    <div class="dropdown-divider"></div>
    @include('people::nav-item.dropdown.menu.item.import-csv')
</div>
