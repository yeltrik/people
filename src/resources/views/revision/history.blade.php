<ul class="list-group">
    <li class="list-group-item active">Revision History</li>
    @foreach($revisionHistory as $history)
        @if($history->key == 'created_at' && !$history->old_value)
            <li class="list-group-item">{{ $history->userResponsible()->name }} created this resource
                at {{ $history->newValue() }}</li>
        @else
            <li class="list-group-item">{{ $history->userResponsible()->name }}
                changed {{ $history->fieldName() }} from {{ $history->oldValue() }} to {{ $history->newValue() }}</li>
        @endif
    @endforeach
</ul>
