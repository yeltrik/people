@extends('layouts.app')

@section('content')
    <div class="container">
        @include('people::people.breadcrumb.index')
        @include('people::people.index.table')

        @include('people::people.index.button.create')
    </div>
@endsection
