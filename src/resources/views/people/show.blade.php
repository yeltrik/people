@extends('layouts.app')

@section('content')
    <div class="container">
        @include('people::people.breadcrumb.show')

        @if($data->count() > 0)
            TODO: Show Data/Datum...
            @foreach($data as $data)
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">Data</h5>

                        @include('people::revision.history', ['revisionHistory' => $data->revisionHistory])

                        TODO: Option to Add Datum

                        <?php $model = $data ?>
                        @if( !isset($model) )
                            {{dd('Model must be provided to associate Data') }}
                        @else
                            <?php $interpreting_class_name = get_class($model) ?>
                            <?php $model_id = $model->getKey() ?>
                        @endif

                        <form
                            method="POST"
                            action="{{ action([\Yeltrik\Data\app\http\controllers\DatumController::class, 'store']) }}"
                        >
                            @csrf

                            <input type="hidden" name="data" value="{{$data}}"> <br>
                            Key: <input type="text" name="key"> <br>
                            Value: <input type="text" name="value"> <br>

                            <button class="btn btn-info">
                                Save Datum
                            </button>
                        </form>

                    </div>
                </div>
                @endforeach
        @else
            @include('data::data.create.form-button')
        @endif

        @include('people::revision.history')
    </div>
@endsection
