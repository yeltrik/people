<form
    method="POST"
    action="{{ action([\Yeltrik\People\app\http\controllers\PersonController::class, 'store']) }}"
>
    @csrf

{{--    @if ($errors->any())--}}
{{--        <div class="alert alert-danger">--}}
{{--            <ul>--}}
{{--                @foreach ($errors->all() as $error)--}}
{{--                    <li>{{ $error }}</li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    @endif--}}

    @include('people::people.create.button.create')

</form>
