<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Name</th>
{{--        <th scope="col">Data</th>--}}
        <th scope="col">View</th>
    </tr>
    </thead>

    <tbody>
    @if(empty($people))
        <tr>
            <th colspan="99">
                <h3 class="text-center">There are no people to list</h3>
            </th>
        </tr>
    @else
        @foreach($people as $person)
            <tr>
                <th scope="row">
                    {{$person->name()}}
                </th>
{{--                <td>--}}
{{--                    @if($person->data() instanceof \Yeltrik\Data\app\models\Data)--}}
{{--                    <a--}}
{{--                        href="#"--}}
{{--                        href="{{route('data.show', $person->data())}}"--}}
{{--                        target="data_{{$person->data()->id}}"--}}
{{--                    >--}}
{{--                        Title Preview of Data--}}
{{--                        Data--}}
{{--                    </a>--}}
{{--                    @else--}}
{{--                        Create Form/Button--}}
{{--                        N/A--}}
{{--                    @endif--}}
{{--                </td>--}}
                <td>
                    <a
                        href="{{route('people.show', $person)}}"
                    >
                        View
                    </a>
                </td>
            </tr>
        @endforeach
    @endif

    </tbody>
</table>
