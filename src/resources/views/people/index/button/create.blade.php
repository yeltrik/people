<a href="{{route('people.create')}}" class="btn btn-block btn-info">
    @include('people::people.icon.add')
    Create
</a>
