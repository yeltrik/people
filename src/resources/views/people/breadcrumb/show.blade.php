<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @include('people::breadcrumb.index')
        @include('people::people.breadcrumb.people')
        @include('people::people.breadcrumb.person')
    </ol>
</nav>
